# README #

### What is this repository for? ###

This repository displays the code for a diversity calendar app. 
* Der Diversity Kalender dient der Darstellung und Übersicht von religiösen und säkularen Feiertagen verschiedenster Kulturen.
* Version 1.0

### How do I get set up? ###

* Fork it!
* Create your feature branch: git checkout -b my-new-feature
* Commit your changes: git commit -am 'Add some feature'
* Push to the branch: git push origin my-new-feature
* Submit a pull request

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or directly write an email to s0551814@htw-berlin.de