package com.example.diversitykalender.activities_and_fragments;


import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.diversitykalender.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isNotChecked;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;


@RunWith(AndroidJUnit4.class)
public class VisibilityUncheckTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);


    @Test
    public void mainActivityTest2() {

        onView(allOf(withId(R.id.list_with_three_columns), withText(R.string.test_date),
                isDisplayed()));

        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());

        onView(allOf(withId(R.id.title), withText(R.string.settings_activity_name), isDisplayed())).perform(click());

        onView(allOf(withId(R.id.rowTextView), withText(R.string.test_category), isDisplayed())).perform(click());

        onData(anything())
                .inAdapterView(withId(R.id.list))
                .atPosition(0)
                .onChildView(withId(R.id.checkBox))
                .check(matches(isDisplayed())).perform(click());

        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());

        onView(allOf(withId(R.id.title), withText(R.string.app_name), isDisplayed())).perform(click());

        onView(withText(R.string.test_date)).check(doesNotExist());

        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());

        onView(allOf(withId(R.id.title), withText(R.string.settings_activity_name), isDisplayed())).perform(click());

        onView(allOf(withId(R.id.rowTextView), withText(R.string.test_category), isDisplayed())).perform(click());

        onData(anything())
                .inAdapterView(withId(R.id.list))
                .atPosition(0)
                .onChildView(withId(R.id.checkBox))
                .check(matches(isNotChecked())).perform(click());

        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());

        onView(allOf(withId(R.id.title), withText(R.string.app_name), isDisplayed())).perform(click());

        onView(allOf(withId(R.id.list_with_three_columns), withText(R.string.test_date),
                isDisplayed()));

    }

}
