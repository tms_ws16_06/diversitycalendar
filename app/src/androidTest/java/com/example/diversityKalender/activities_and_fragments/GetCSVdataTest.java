package com.example.diversitykalender.activities_and_fragments;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.test.rule.ActivityTestRule;

import com.example.diversitykalender.R;
import com.example.diversitykalender.data.SQLiteDatabaseHelper;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

/**
 * Created by l on 06.02.17.
 * This testclass tests if the data is retrieved correctly from the internet when there is no local
 * database yet
 */

public class GetCSVdataTest {


    @Rule
    public final ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class, false);

    @Before
    public void beforeActivityLaunched() throws Exception {
        Context thisContext = mActivityRule.getActivity().getBaseContext();
        SQLiteDatabaseHelper dbHelper = new SQLiteDatabaseHelper(thisContext);
        SQLiteDatabase sqLiteDatabase = dbHelper.getWritableDatabase();
        dbHelper.onUpgrade(sqLiteDatabase, 1, 1);
    }

    @Test
    public void checkErrorHandling() throws Exception {
        Intent intent = new Intent();
        mActivityRule.launchActivity(intent);

        onView(allOf(withId(R.id.ThirdColumn), withText(R.string.category_header),
                isDisplayed()));
        Context thisContext = mActivityRule.getActivity().getBaseContext();
        SQLiteDatabaseHelper dbHelper = new SQLiteDatabaseHelper(thisContext);
        Assert.assertTrue(thisContext.getString(R.string.error_if_assert_not_works), dbHelper.checkIfDbUpToDate());
    }
}
