package com.example.diversitykalender.activities_and_fragments;

import android.content.ComponentName;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;

import com.example.diversitykalender.R;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.InstrumentationRegistry.getTargetContext;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.times;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;

import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

/**
 * Created by l on 05.02.17.
 * This class tests the navigation through the app by choosing different activities from the optionsmenu.
 */

public class BasicOptionsMenuTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);


    @Test
    public void ensureActivityChangesWork() {
        Intents.init();
        onView(withId(R.id.main_layout));

        //switch to SettingsActivity
        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());
        onView(withText(R.string.settings_activity_name)).perform(click());
        intended(hasComponent(new ComponentName(getTargetContext(), SettingsActivity.class)));

        //switch to AboutActivity
        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());
        onView(withText(R.string.about_activity_name)).perform(click());
        intended(hasComponent(new ComponentName(getTargetContext(), AboutActivity.class)));

        //switch to MainActivity
        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());
        onView(withText(R.string.app_name)).perform(click());
        onView(allOf(withId(R.id.ThirdColumn), withText(R.string.category_header), isDisplayed()));

        //switch to AboutActivity
        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());
        onView(withText(R.string.about_activity_name)).perform(click());
        intended(hasComponent(new ComponentName(getTargetContext(), AboutActivity.class)), times(2));

        //switch to SettingsActivity
        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());
        onView(withText(R.string.settings_activity_name)).perform(click());
        intended(hasComponent(new ComponentName(getTargetContext(), SettingsActivity.class)), times(2));
        Intents.release();
    }

}
