package com.example.diversitykalender.data;

import com.example.diversitykalender.R;

/**
 * Created by l on 09.12.16.
 * This class represents the modell of the data retrieved from Koellns Open Data website
 */

public class CalendarEntries {

    private final long id;
    private final String datum;
    private final String feiertag;
    private final Category category;
    private Boolean visibility;


    public CalendarEntries(long id, String datum, String feiertag, Category category, Boolean visibility) {
        this.id = id;
        this.datum = datum;
        this.feiertag = feiertag;
        this.category = category;
        this.visibility = visibility;
    }

    public String toString() {
        return R.string.date_header + datum + R.string.title_header + feiertag + R.string.category_header + category.toString() + "\n";
    }

    public long getId() {
        return id;
    }

    public String getDatum() {
        return datum;
    }

    public String getFeiertag() {
        return feiertag;
    }

    public String getCategory() {
        return category.toString();
    }

    public Boolean getVisiblity() {
        return visibility;
    }

    public void setVisiblity(Boolean visible) {
        visibility = visible;
    }

}
