package com.example.diversitykalender.data;


import android.support.annotation.NonNull;


/**
 * Created by l on 20.01.17.
 * this class represents the category; originally implemented to give each category a certain symbol
 * or color; for the moment obsolete and could become replaced by a string attribute in CalendarEntries
 */

public class Category implements Comparable<Category> {

    private final String m_name;

    public Category(String name) {
        m_name = name;
    }

    @Override
    public int compareTo(@NonNull Category category) {
        return this.m_name.compareToIgnoreCase(category.m_name);
    }

    public String toString() {
        return m_name;
    }

}
