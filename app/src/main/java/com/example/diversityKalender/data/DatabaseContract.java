package com.example.diversitykalender.data;

import android.provider.BaseColumns;

/**
 * Created by l on 24.01.17.
 * This class displays the structure and some queries for the local SQLite database where the
 * calendar entries are stored after retrieval from the website
 */

public final class DatabaseContract {


        private DatabaseContract() {
        }

        public static class Calendar implements BaseColumns {

            public static final String TABLE_NAME = "calendar";
            public static final String ID = "_id";
            public static final String COLUMN_DATE = "date";
            public static final String COLUMN_TITLE = "title";
            public static final String COLUMN_VISIBILITY = "visibility";
            public static final String COLUMN_CATEGORY = "category";

            public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS "
                    + TABLE_NAME
                    + " ("
                    + ID
                    + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + COLUMN_DATE + " TEXT NOT NULL, "
                    + COLUMN_TITLE + " TEXT NOT NULL, "
                    + COLUMN_CATEGORY + " TEXT NOT NULL, "
                    + COLUMN_VISIBILITY
                    + " INTEGER DEFAULT 1"
                    + ");";

            private static final String SELECT_FROM = "SELECT * FROM ";
            private static final String WHERE = " WHERE ";
            private static final String EQUALS = " = ";
            private static final String UPDATE = "UPDATE ";
            private static final String SET_VISIBILITY = " SET visibility = ";
            private static final String SEMICOLON = ";";
            private static final String WHERE_VISIBILITY_TRUE = " = 1";
            private static final String STRINGSIGNFRONT = " '";
            private static final String STRINGSIGNBACK = "' ";

            public static String getUpdateVisibilityQuery(Boolean visible, long id) {
                int visibility = visible ? 1 : 0;
                return UPDATE + TABLE_NAME + SET_VISIBILITY + visibility + WHERE + ID + EQUALS + id + SEMICOLON;
            }

            public static String getVisibleEntries() {
              return SELECT_FROM + DatabaseContract.Calendar.TABLE_NAME + WHERE + COLUMN_VISIBILITY + WHERE_VISIBILITY_TRUE + SEMICOLON;
            }

            public static String getSpecificCategoryEntries(String category) {
                return SELECT_FROM + DatabaseContract.Calendar.TABLE_NAME + WHERE + COLUMN_CATEGORY + EQUALS + STRINGSIGNFRONT + category + STRINGSIGNBACK + SEMICOLON;
            }

        }
}
