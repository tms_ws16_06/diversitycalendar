package com.example.diversitykalender.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.diversitykalender.R;
import com.example.diversitykalender.activities_and_fragments.MainActivity;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by l on 24.01.17.
 * This is a helper class in android the establish a connection to the local database using the
 * database contract class for the queries
 */

public class SQLiteDatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "diversity_calendar.db";
    private static final String SELECT_FROM = "SELECT * FROM ";
    private static final String DROP_TABLE = "DROP TABLE IF EXISTS ";

    public SQLiteDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DatabaseContract.Calendar.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        onDropTable(sqLiteDatabase);
        onCreate(sqLiteDatabase);
    }


    private void onDropTable(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DROP_TABLE + DatabaseContract.Calendar.TABLE_NAME);
    }

    public void onUpdate(SQLiteDatabase sqLiteDatabase, String query) {
        sqLiteDatabase.execSQL(query);
    }

    public ArrayList<CalendarEntries> getEntries(String query) {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<CalendarEntries> listItems = new ArrayList<>();
       Log.d(getClass().getName(), ": following query gets executed: " + query);
        Cursor cursor = db.rawQuery(query,
                new String[]{});

        if (cursor.moveToFirst()) {
            Boolean categoryDuplicate;
            String date;
            String title;
            String categoryName;
            Boolean visibility;
            int visibilityInteger;
            do {
                categoryDuplicate = false;
                CalendarEntries calendarEntries = null;
                long id = cursor.getLong(cursor.getColumnIndex(DatabaseContract.Calendar.ID));
                date = cursor.getString(cursor.getColumnIndex(DatabaseContract.Calendar.COLUMN_DATE));
                title = cursor.getString(cursor.getColumnIndex(DatabaseContract.Calendar.COLUMN_TITLE));
                categoryName = cursor.getString(cursor.getColumnIndex(DatabaseContract.Calendar.COLUMN_CATEGORY));
                visibilityInteger = cursor.getInt(cursor.getColumnIndex(DatabaseContract.Calendar.COLUMN_VISIBILITY));
                visibility = visibilityInteger == 1;
               // Log.d(getClass().getName(), ": Cursor has returned: " + id + ", " + date + ", " + title + ", " + categoryName + ", visibility = " + visibility);
                for (Category category : MainActivity.getTreeSet()) {
                    if (category.toString().equals(categoryName)) {
                        calendarEntries = new CalendarEntries(id, date, title, category, visibility);
                        categoryDuplicate = true;
                    }
                }
                if (!categoryDuplicate) {
                    Category newCategory = new Category(categoryName);
                    MainActivity.getTreeSet().add(newCategory);
                    calendarEntries = new CalendarEntries(id, date, title, newCategory, visibility);
                }
                listItems.add(calendarEntries);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return listItems;
    }

    public ArrayList<CalendarEntries> getAllEntries() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<CalendarEntries> listItems = new ArrayList<>();

        Cursor cursor = db.rawQuery(SELECT_FROM + DatabaseContract.Calendar.TABLE_NAME,
                new String[]{});

        if (cursor.moveToFirst()) {
            Boolean categoryDuplicate;
            String date;
            String title;
            String categoryName;
            Boolean visibility;
            int visibilityInteger;
            do {
                categoryDuplicate = false;
                CalendarEntries calendarEntries = null;
                long id = cursor.getLong(cursor.getColumnIndex(DatabaseContract.Calendar.ID));
                date = cursor.getString(cursor.getColumnIndex(DatabaseContract.Calendar.COLUMN_DATE));
                title = cursor.getString(cursor.getColumnIndex(DatabaseContract.Calendar.COLUMN_TITLE));
                categoryName = cursor.getString(cursor.getColumnIndex(DatabaseContract.Calendar.COLUMN_CATEGORY));
                visibilityInteger = cursor.getInt(cursor.getColumnIndex(DatabaseContract.Calendar.COLUMN_VISIBILITY));
                visibility = visibilityInteger == 1;
               // Log.d(getClass().getName(), ": Cursor has returned: " + id + ", " + date + ", " + title + ", " + categoryName + ", visibility = " + visibility);
                for (Category category : MainActivity.getTreeSet()) {
                    if (category.toString().equals(categoryName)) {
                        calendarEntries = new CalendarEntries(id, date, title, category, visibility);
                        categoryDuplicate = true;
                    }
                }
                if (!categoryDuplicate) {
                    Category newCategory = new Category(categoryName);
                    MainActivity.getTreeSet().add(newCategory);
                    calendarEntries = new CalendarEntries(id, date, title, newCategory, visibility);
                }
                listItems.add(calendarEntries);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return listItems;
    }

    public Boolean checkIfDbUpToDate() {
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(SELECT_FROM + DatabaseContract.Calendar.TABLE_NAME, null);
        if (!cursor.moveToFirst()) {
            cursor.close();
            return false;
        } else {
            String date = cursor.getString(cursor.getColumnIndex(DatabaseContract.Calendar.COLUMN_DATE));
            String[] dbDate = date.split("\\.");
            java.util.Calendar now = Calendar.getInstance();
            int year = now.get(Calendar.YEAR);
            String yearToday = Integer.toString(year);
            cursor.close();
            if (yearToday.equals(dbDate[2])) {
                return true;
            } else {
                return false;
            }
        }
    }

}
