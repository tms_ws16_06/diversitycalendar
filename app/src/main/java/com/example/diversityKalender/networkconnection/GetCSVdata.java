package com.example.diversitykalender.networkconnection;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.ListView;

import com.example.diversitykalender.activities_and_fragments.MainActivity;
import com.example.diversitykalender.R;
import com.example.diversitykalender.adapter.ListViewAdapter;
import com.example.diversitykalender.data.Category;
import com.example.diversitykalender.data.DatabaseContract;
import com.example.diversitykalender.data.SQLiteDatabaseHelper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by l on 20.01.17.
 * This class is called by the main activity when there are no entries in the database yet and the
 * csv data still has to become fetched from the koelln open data side.
 * It uses an asynchronous thread to implement the internet connection.
 */

public class GetCSVdata {

    private ListView m_listView;
    private final Activity m_activity;
    private final AlertDialog mAlertDialog;
    private static final int READ_TIMEOUT = 10000;
    private static final int CONNECT_TIMEOUT = 15000;
    private static final int RESPONSE_CODE_SUCCESS = 200;

    public GetCSVdata(Activity activity, AlertDialog alertDialog) {
        this.m_activity = activity;
        mAlertDialog = alertDialog;
        mAlertDialog.setTitle("Info");
    }

    public void getData(ListView listview) {
        m_listView = listview;
        ConnectivityManager connMgr = (ConnectivityManager)
                m_activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            mAlertDialog.setMessage(m_activity.getString(R.string.downloading_data));
            mAlertDialog.show();
            new DownloadCVSdata().execute();
            mAlertDialog.cancel();
        } else {
            try {
                mAlertDialog.setMessage(m_activity.getString(R.string.message_no_internetconnectivity));
                mAlertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                mAlertDialog.setButton(DialogInterface.BUTTON_POSITIVE, m_activity.getString(R.string.ok_button), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        m_activity.finish();

                    }
                });
                mAlertDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d(GetCSVdata.class.getName(), m_activity.getString(R.string.log_network_connection_failed));
        }
    }

    /**
     * makes an asynchronous request from URL
     */
    private class DownloadCVSdata extends AsyncTask<String, Void, Boolean> {
        @Override
        protected Boolean doInBackground(String... urls) {
            downloadUrl();
            return true;
        }

        // Given a URL, establishes an HttpUrlConnection and retrieves
        // the web page content as a InputStream, which it returns as
        // a string.
        private void downloadUrl() {
            try {

                URL url = new URL(m_activity.getString(R.string.urlText));
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT /* milliseconds */);
                conn.setConnectTimeout(CONNECT_TIMEOUT /* milliseconds */);
                conn.setRequestMethod(m_activity.getString(R.string.http_get_label));
                conn.setDoInput(true);
                // Starts the query
                conn.connect();
                int responseCode = conn.getResponseCode();
                if (responseCode != RESPONSE_CODE_SUCCESS) {
                    throw new IOException(m_activity.getString(R.string.response_failed));
                } else {
                    InputStream inputStream = conn.getInputStream();
                    loadCalendarEntries(inputStream);
                    if (inputStream != null) {
                        inputStream.close();
                    }
                    conn.disconnect();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        public void loadCalendarEntries(InputStream inputStream) {
            Log.d(getClass().getName(), m_activity.getString(R.string.moving_data_from_csv_to_db));

            String date;
            String title;
            String categoryName;
            Boolean categoryDuplicate = false;

            String read;
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            SQLiteDatabase database = new SQLiteDatabaseHelper(m_activity).getWritableDatabase();

            try {
                while ((read = br.readLine()) != null) {
                    String[] rowData = read.split(m_activity.getString(R.string.csv_splitter));
                    try {
                        categoryName = rowData[2];
                    } catch (IndexOutOfBoundsException e) {
                        categoryName = m_activity.getString(R.string.unknown_category);
                    }
                    if (!categoryName.equals(m_activity.getString(R.string.religion_entry_in_csv))) {
                        date = rowData[0];
                        title = rowData[1];
                        for (Category category : MainActivity.getTreeSet()) {
                            if (category.toString().equals(categoryName)) {
                                ContentValues values = new ContentValues();
                                values.put(DatabaseContract.Calendar.COLUMN_DATE, date);
                                values.put(DatabaseContract.Calendar.COLUMN_TITLE, title);
                                values.put(DatabaseContract.Calendar.COLUMN_CATEGORY, category.toString());
                                database.insert(DatabaseContract.Calendar.TABLE_NAME, null, values);
                                categoryDuplicate = true;
                            }
                        }
                        if (!categoryDuplicate) {
                            Category newCategory = new Category(categoryName);
                            MainActivity.getTreeSet().add(newCategory);
                            ContentValues values = new ContentValues();
                            values.put(DatabaseContract.Calendar.COLUMN_DATE, date);
                            values.put(DatabaseContract.Calendar.COLUMN_TITLE, title);
                            values.put(DatabaseContract.Calendar.COLUMN_CATEGORY, newCategory.toString());
                            database.insert(DatabaseContract.Calendar.TABLE_NAME, null, values);
                        }
                        categoryDuplicate = false;
                    }
                }
                database.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(Boolean success) {
            Log.d(getClass().getName(), m_activity.getString(R.string.listview_filled));
            SQLiteDatabaseHelper dbHelper = new SQLiteDatabaseHelper(m_activity);
            ListViewAdapter adapter = new ListViewAdapter(m_activity, dbHelper.getAllEntries());
            dbHelper.close();
            m_listView.setAdapter(adapter);
            mAlertDialog.cancel();
        }
    }

}


