package com.example.diversitykalender.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.diversitykalender.data.CalendarEntries;
import com.example.diversitykalender.R;

import java.util.ArrayList;


/**
 * Created by l on 06.01.17.
 * This adapter is used for the main activity to fill the listview with the data from the database
 * in three columns: date, calendarentry-title, category
 */


public class ListViewAdapter extends BaseAdapter {

    private final ArrayList<CalendarEntries> list;
    private final Context m_context;

    public ListViewAdapter(Context context, ArrayList<CalendarEntries> list) {
        super();
        m_context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).getId();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CalendarEntries calendarEntry = list.get(position);
        LayoutInflater inflater = LayoutInflater.from(m_context);
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_with_three_columns, null);
            viewHolder.first = (TextView) convertView.findViewById(R.id.FirstColumn);
            viewHolder.second = (TextView) convertView.findViewById(R.id.SecondColumn);
            viewHolder.third = (TextView) convertView.findViewById(R.id.ThirdColumn);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.first.setText(calendarEntry.getDatum());
        viewHolder.second.setText(calendarEntry.getFeiertag());
        viewHolder.third.setText(calendarEntry.getCategory());

        return convertView;
    }

    private static class ViewHolder {
        TextView first;
        TextView second;
        TextView third;
    }

}