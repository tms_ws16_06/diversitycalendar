package com.example.diversitykalender.adapter;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.diversitykalender.data.CalendarEntries;
import com.example.diversitykalender.R;
import com.example.diversitykalender.data.DatabaseContract;
import com.example.diversitykalender.data.SQLiteDatabaseHelper;

import java.util.ArrayList;

/**
 * Created by l on 20.01.17.
 * Adapter that adds to the listview a checkbox and displays only date and calendarEntry title
 * as it is used in the fragment of the settings activity, where the entries are sorted by category
 * already
 */

public class CheckboxListAdapter extends BaseAdapter {

    private final ArrayList<CalendarEntries> list;

    private final Context m_context;


    public CheckboxListAdapter(Context context, ArrayList<CalendarEntries> list) {
        super();
        m_context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).getId();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(m_context);
        CheckboxListAdapter.ViewHolder viewHolder;

        if (convertView == null) {

            viewHolder = new CheckboxListAdapter.ViewHolder();

            convertView = inflater.inflate(R.layout.list_checkbox_fragment, null);
            viewHolder.first = (TextView) convertView.findViewById(R.id.FirstColumn);
            viewHolder.second = (TextView) convertView.findViewById(R.id.SecondColumn);
            viewHolder.checkbox = (CheckBox) convertView.findViewById(R.id.checkBox);
            convertView.setTag(viewHolder);
            viewHolder.checkbox.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {

                    CheckBox checkBox = (CheckBox) v;
                    CalendarEntries calendarEntry = (CalendarEntries) checkBox.getTag();
                   // Log.d(getClass().getName(), ": CalendarEntry : " + calendarEntry.getFeiertag() + " is set Visible: " + checkBox.isChecked());
                    calendarEntry.setVisiblity(checkBox.isChecked());
                    SQLiteDatabaseHelper dbHelper = new SQLiteDatabaseHelper(m_context);
                    SQLiteDatabase sqLiteDatabase = dbHelper.getReadableDatabase();
                    dbHelper.onUpdate(sqLiteDatabase, DatabaseContract.Calendar.getUpdateVisibilityQuery(checkBox.isChecked(), calendarEntry.getId()));
                }
            });
        } else {
            viewHolder = (CheckboxListAdapter.ViewHolder) convertView.getTag();
        }

        CalendarEntries calenderEntry = list.get(position);
        viewHolder.first.setText(calenderEntry.getDatum());
        viewHolder.second.setText(calenderEntry.getFeiertag());
        viewHolder.checkbox.setChecked(calenderEntry.getVisiblity());
        viewHolder.checkbox.setTag(calenderEntry);

        return convertView;
    }

    private static class ViewHolder {
        TextView first;
        TextView second;
        CheckBox checkbox;
    }

}
