package com.example.diversitykalender.activities_and_fragments;


import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;

import com.example.diversitykalender.R;
import com.example.diversitykalender.adapter.ListViewAdapter;
import com.example.diversitykalender.data.CalendarEntries;
import com.example.diversitykalender.data.Category;
import com.example.diversitykalender.data.DatabaseContract;
import com.example.diversitykalender.data.SQLiteDatabaseHelper;
import com.example.diversitykalender.networkconnection.GetCSVdata;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeSet;

/**
 * Tests
 * UnitTest: ob Fehlermeldung, wenn Internetübertragung nicht funktioniert
 * Ablauf-Test: ob Objekte wirklich nicht angezeigt werden, wenn Checkbox unmarkiert
 * Oberflächent-Test: werden Optionsmenüs korrekt angezeigt?
 */

public class MainActivity extends AppCompatActivity {

    private static TreeSet<Category> treeSet;
    private ListView listView;
    private AlertDialog alertDialog;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.mainListView);

        SQLiteDatabaseHelper dbHelper = new SQLiteDatabaseHelper(this);

        //only to check if getCSVdata works:
      /*  SQLiteDatabase database = dbHelper.getWritableDatabase();
        dbHelper.onUpgrade(database, 1, 2);*/

        if (dbHelper.checkIfDbUpToDate()) {
            Log.d(getClass().getName(), getString(R.string.log_db_exists));
            ArrayList<CalendarEntries> calendarEntries = dbHelper.getEntries(DatabaseContract.Calendar.getVisibleEntries());
            ListViewAdapter adapter = new ListViewAdapter(this, calendarEntries);
            listView.setAdapter(adapter);
        } else {
            Log.d(getClass().getName(), getString(R.string.log_db_not_exists));
            alertDialog = new AlertDialog.Builder(this).create();
            GetCSVdata csvData = new GetCSVdata(this, alertDialog);
            csvData.getData(listView);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_action_bar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.main_action_menu_settings:
                intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.main_action_menu_about:
                intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        SQLiteDatabaseHelper dbHelper = new SQLiteDatabaseHelper(this);
        Log.d(getClass().getName(), getString(R.string.log_db_exists));
        ArrayList<CalendarEntries> calendarEntries = dbHelper.getEntries(DatabaseContract.Calendar.getVisibleEntries());
        dbHelper.close();
        ListViewAdapter adapter = new ListViewAdapter(this, calendarEntries);
        listView.setAdapter(adapter);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public void onDestroy() {
        if (alertDialog != null) {
            if (alertDialog.isShowing()) {
                alertDialog.cancel();
            }
        }
        super.onDestroy();
    }

    public static TreeSet<Category> getTreeSet() {
        if (treeSet == null) {
            treeSet = new TreeSet<>(new Comparator<Category>() {
                public int compare(Category stud1, Category stud2) {
                    return stud1.compareTo(stud2);
                }
            });
        }
        return treeSet;
    }

}
