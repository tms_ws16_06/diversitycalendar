package com.example.diversitykalender.activities_and_fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;


import com.example.diversitykalender.R;
import com.example.diversitykalender.data.Category;

import java.util.ArrayList;
import java.util.List;

/**
 *  Created by l on 13.01.17.
 *  This class represents the settings activity where it's possible to choose the calendar entries
 *  to become displayed on the mainactivity
 */

public class SettingsActivity extends AppCompatActivity {


    /**
     * onCreate calls method to fill first listview of activity and to add first fragment
     * @param savedInstanceState contains informations about the
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(SettingsActivity.class.getName(), getString(R.string.log_oncreate_called));

        setContentView(R.layout.activity_settings);
        addCalendarCategories();

        if (findViewById(R.id.fragment_container) != null) {

            if (savedInstanceState != null) {
                return;
            }
            CalendarFragment firstFragment = new CalendarFragment();
            Bundle bundle = new Bundle();
            bundle.putString(getString(R.string.category_tag), MainActivity.getTreeSet().first().toString());
            firstFragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, firstFragment).commit();
        }
    }

    /**
     * method to add the calendarCategories to the upper listView and to add the onClickListener,
     * that arranges the switch of the fragment and it's listview of calendar entries if other
     * category is chosen
     */
    private void addCalendarCategories() {

        ListView listView = (ListView) findViewById(R.id.buttonPanel);
        listView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        finish();
                        return true;
                    }
                }
                return false;
            }
        });

        List<String> arrayList = new ArrayList<>();

        for (Category category : MainActivity.getTreeSet()) {
            arrayList.add(category.toString());
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                this,
                R.layout.simplerow,
                arrayList);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final TextView txt = (TextView) view.findViewById(R.id.rowTextView);
                String txt2 = txt.getText().toString();
                switchFragment(txt2);
                Log.i(SettingsActivity.class.getName(), ": " + txt2);
            }
        });
        listView.setItemChecked(0, true);
    }

    /**
     * method which is called by onClickListener of upper listView to change content of fragment
     * @param category the category of the holidays to become displayed
     */
    private void switchFragment(String category) {
        Fragment nextFragment = new CalendarFragment();
        Bundle bundle = new Bundle();
        bundle.putString(getString(R.string.category_tag), category);
        nextFragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, nextFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.settings_action_bar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.setting_action_menu_main:
                finish();
                return true;
            case R.id.settings_action_menu_about:
                intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
