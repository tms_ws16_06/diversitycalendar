package com.example.diversitykalender.activities_and_fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.diversitykalender.R;
import com.example.diversitykalender.adapter.CheckboxListAdapter;
import com.example.diversitykalender.data.DatabaseContract;
import com.example.diversitykalender.data.SQLiteDatabaseHelper;


/**
 * Created by l on 13.01.17.
 * This class represents the fragment in the settings activity
 * It is exchanged depending on what has been chosen on the upper listView of that activity, thus
 * it shows the calendar entries of one specific category
 */

public class CalendarFragment extends Fragment {

    //the chosen category of the calendar entries to display
    private String categoryChoice;

    /**
     * all Fragments are instantiated and onCreateView() is called when parentLayout is created
     *
     * @param inflater needed to inflate fragmentView
     * @param parentViewGroup also needed for inflation
     * @param savedInstanceState handed over which Category has been chosen
     * @return rootView of Fragment
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup,
                             Bundle savedInstanceState) {
        Log.d(CalendarFragment.class.getName(), getString(R.string.log_oncreate_called));
        View rootView = inflater.inflate(R.layout.list_fragment, parentViewGroup, false);
        if (getArguments() != null) {
            Bundle bundle = getArguments();
            categoryChoice = bundle.getString(getString(R.string.category_tag));
        }
        return rootView;
    }

    /**
     * called, when fragmentTransaction is started
     * @param savedInstanceState is used in case this fragment has already a saved state
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        SQLiteDatabaseHelper dbHelper = new SQLiteDatabaseHelper(getActivity());
        try {
            ListView listView = (ListView) getView().findViewById(R.id.list);
            CheckboxListAdapter adapter = new CheckboxListAdapter(getActivity(), dbHelper.getEntries(DatabaseContract.Calendar.getSpecificCategoryEntries(categoryChoice)));
            dbHelper.close();
            listView.setAdapter(adapter);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        super.onActivityCreated(savedInstanceState);
    }

}
